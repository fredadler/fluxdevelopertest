﻿using Gaminho;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlGame : MonoBehaviour
{
    public ScenarioLimits ScenarioLimit;
    public Level[] Levels;
    public Image Background;
    [Header("UI")]
    public Text TextStart;
    public Text TextPoints;
    public Transform BarLife;
    public GameObject gamePausedText;
    bool gamePaused = false;
    AudioSource MusicAudioSource;
    // Use this for initialization
    void Start()
    {
        if (Statics.Continue) LoadGame();
        Statics.EnemiesDead = 0;
        Background.sprite = Levels[Statics.CurrentLevel].Background;
        TextStart.text = "Stage " + (Statics.CurrentLevel + 1);
        GetComponent<AudioSource>().PlayOneShot(Levels[Statics.CurrentLevel].AudioLvl);
        MusicAudioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        TextPoints.text = Statics.Points.ToString();
        BarLife.localScale = new Vector3(Statics.Life / 10f, 1, 1);

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            gamePaused = !gamePaused;
            Time.timeScale = gamePaused ? 0 : 1;
            MusicAudioSource.mute = gamePaused;
            gamePausedText.SetActive(gamePaused);
        }
    }

    public void LevelPassed()
    {

        Clear();
        Statics.CurrentLevel++;
        Statics.Points += 1000 * Statics.CurrentLevel;
        SaveGame();
        if (Statics.CurrentLevel < 3)
        {
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_LEVELUP) as GameObject);
        }
        else
        {
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_CONGRATULATION) as GameObject);
        }
    }
    //Oops, when you lose (: Starts from Zero
    public void GameOver()
    {
        SaveGame();
        BarLife.localScale = new Vector3(0, 1, 1);
        Clear();
        Destroy(Statics.Player.gameObject);
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_GAMEOVER) as GameObject);
    }
    private void Clear()
    {
        GetComponent<AudioSource>().Stop();
        GameObject[] Enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject ini in Enemies)
        {
            Destroy(ini);
        }
    }

    public void SaveGame()
    {
        SaveGameData saveGameData = new SaveGameData();
        saveGameData.LastLevel = Statics.CurrentLevel;
        string fileName = "SaveGame.json";
        string filePath = Path.Combine(Application.streamingAssetsPath, fileName);

        string data = JsonUtility.ToJson(saveGameData);

        if (!File.Exists(filePath))
            File.Create(filePath);

        File.WriteAllText(filePath, data);
    }

    public void LoadGame()
    {
        Statics.Continue = false;
        string fileName = "SaveGame.json";
        string filePath = Path.Combine(Application.streamingAssetsPath, fileName);

        string dataAsJson = File.ReadAllText(filePath);

        SaveGameData saveGameData = JsonUtility.FromJson<SaveGameData>(dataAsJson);

        Statics.CurrentLevel = saveGameData.LastLevel;
    }
}

[System.Serializable]
public class SaveGameData
{
    public int LastLevel;
}