using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemyShip
{
    void IncreaseFiringSpeed(float increaseBy);
}
